#!/bin/bash

while true; do
    clear
    echo "Menu:"
    echo "a -> Executar ping para um host (ip ou site)."
    echo "b -> Listar os usuários atualmente logados na máquina."
    echo "c -> Exibir o uso de memória e de disco da máquina."
    echo "d -> Sair."

    read -p "Escolha uma opção (a/b/c/d): " choice

    case $choice in
        
            read -p "Digite o host (ip ou site): " host
            ping -c 4 $host
            read -p "Pressione Enter para continuar..."
            ;;
        
            who
            read -p "Pressione Enter para continuar..."
            ;;
        
            echo "Uso de memória:"
            free -m
            echo -e "\nUso de disco:"
            df -h
            read -p "Pressione Enter para continuar..."
            ;;
        
            echo "Saindo..."
            exit 0
            ;;
        
            echo "Opção inválida. Tente novamente."
            read -p "Pressione Enter para continuar..."
            ;;
    esac
done
